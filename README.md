# ESP Import

Given a mirror of the Elite Sport Performance course in `html/*.html`
do a simple "wikification" of it and upload it to the wiki specified
by the `APIURL` constant.

A `wikieducator.rc` ini file is presumed to be in the user's home
directory containing a `[login]` section specifying the user and
password.



