/*
 * import the Elite Sport Performance course into the wiki
 *
 * Copyright 2016 Open Education Resource Foundation
 * @license MIT
 */
"use strict";
const //SUMMARY = 'Uploaded from http://www.elitesportpsy.org.au Copyright 2015 USQ - Peter Terry and Neil Martin. CC BY-4.0',
      SUMMARY = 'improve slider/carousel HTML->wiki mapping',
      APIURL = 'http://wikieducator.org/api.php';

var fs = require('fs'),
    path = require('path'),
    util = require('util'),
    untildify = require('untildify'),
    ini = require('ini'),
    request = require('request'),
    cheerio = require('cheerio');

var urls = {},
    files = [],
    pages = [
    'Elite sport performance/Welcome|Welcome',
    'Elite sport performance/Introduction',
    'Elite sport performance/Introduction/Introducing psychological skills|Introducing psychological skills',
    'Elite sport performance/Introduction/Categories of psychological influence|Categories of psychological influence',
    'Elite sport performance/Introduction/Sport psychologist|Sport psychologist',
    'Elite sport performance/Introduction/The six pillars of elite sports performance|The six pillars of elite sports performance',
    'Elite sport performance/Motivation',
    'Elite sport performance/Motivation/Motivation|Motivation',
    'Elite sport performance/Motivation/Describing motivation|Describing motivation',
    'Elite sport performance/Motivation/Motivational strategies|Motivational strategies',
    'Elite sport performance/Motivation/Token rewards|Token rewards',
    'Elite sport performance/Motivation/Motivation summary|Motivation summary',
    'Elite sport performance/Anxiety',
    'Elite sport performance/Anxiety/Anxiety|Anxiety',
    'Elite sport performance/Anxiety/Controlling anxiety|Controlling anxiety',
    'Elite sport performance/Anxiety/Responses to stress|Responses to stress',
    'Elite sport performance/Anxiety/Relaxation techniques|Relaxation techniques',
    'Elite sport performance/Anxiety/Anxiety summary|Anxiety summary',
    'Elite sport performance/Mood and emotion',
    'Elite sport performance/Mood and emotion/Mood and emotion|Mood and emotion',
    'Elite sport performance/Mood and emotion/Mood emotion|Mood emotion',
    'Elite sport performance/Mood and emotion/Your mood profile|Your mood profile',
    'Elite sport performance/Mood and emotion/Mood regulation strategies|Mood regulation strategies',
    'Elite sport performance/Mood and emotion/Mood summary and go further|Mood summary and go further',
    'Elite sport performance/Self-confidence',
    'Elite sport performance/Self-confidence/Self-confidence|Self-confidence',
    'Elite sport performance/Self-confidence/Nature of self-confidence|Nature of self-confidence',
    'Elite sport performance/Self-confidence/Threats to self-confidence|Threats to self-confidence',
    'Elite sport performance/Self-confidence/Boosting self-confidence|Boosting self-confidence',
    'Elite sport performance/Self-confidence/Confidence summary and go further|Confidence summary and go further',
    'Elite sport performance/Concentration',
    'Elite sport performance/Concentration/Concentration|Concentration',
    'Elite sport performance/Concentration/Concentration attention|Concentration attention',
    'Elite sport performance/Concentration/Errors and distractions|Errors and distractions',
    'Elite sport performance/Concentration/Concentration training techniques|Concentration training techniques',
    'Elite sport performance/Concentration/Concentration summary and go further|Concentration summary and go further',
    'Elite sport performance/Imagery',
    'Elite sport performance/Imagery/Imagery|Imagery',
    'Elite sport performance/Imagery/Imagery perspectives|Imagery perspectives',
    'Elite sport performance/Imagery/Imagery for skill development|Imagery for skill development',
    'Elite sport performance/Imagery/Imagery in competition|Imagery in competition',
    'Elite sport performance/Imagery/Imagery summary|Imagery summary',
    'Elite sport performance/Music',
    'Elite sport performance/Music/Music|Music',
    'Elite sport performance/Music/Music performance|Music performance',
    'Elite sport performance/Music/Music training|Music training',
    'Elite sport performance/Music/Interventions using music|Interventions using music',
    'Elite sport performance/Music/Music summary and go further|Music summary and go further',
    'Elite sport performance/Group dynamics',
    'Elite sport performance/Group dynamics/Group dynamics|Group dynamics',
    'Elite sport performance/Group dynamics/Introducing group dynamics|Introducing group dynamics',
    'Elite sport performance/Group dynamics/Team culture and distinctiveness|Team culture and distinctiveness',
    'Elite sport performance/Group dynamics/Improving team performance|Improving team performance',
    'Elite sport performance/Group dynamics/Group dynamics summary|Group dynamics summary',
    'Elite sport performance/Mental training',
    'Elite sport performance/Mental training/Mental training|Mental training'
    ],
    targets = {
      'http://www.elitesportpsy.org.au/lessons/anxiety/': 'Elite sport performance/Anxiety/Anxiety',
      'http://www.elitesportpsy.org.au/lessons/concentration/': 'Elite sport performance/Concentration/Concentration',
      'http://www.elitesportpsy.org.au/lessons/group-dynamics/': 'Elite sport performance/Group dynamics/Group dynamics',
      'http://www.elitesportpsy.org.au/lessons/imagery/': 'Elite sport performance/Imagery/Imagery',
      'http://www.elitesportpsy.org.au/lessons/mood-and-emotion/': 'Elite sport performance/Mood and emotion/Mood and emotion',
      'http://www.elitesportpsy.org.au/lessons/motivation/': 'Elite sport performance/Motivation',
      'http://www.elitesportpsy.org.au/lessons/music/': 'Elite sport performance/Music',
      'http://www.elitesportpsy.org.au/lessons/self-confidence/': 'Elite sport performance/Self-confidence',
      'http://www.elitesportpsy.org.au/modules/anxiety/': 'Elite sport performance/Anxiety/Anxiety',
      'http://www.elitesportpsy.org.au/modules/concentration/': 'Elite sport performance/Concentration/Concentration',
      'http://www.elitesportpsy.org.au/modules/group-dynamics/': 'Elite sport performance/Group dynamics/Group dynamics',
      'http://www.elitesportpsy.org.au/modules/imagery/': 'Elite sport performance/Imagery/Imagery',
      'http://www.elitesportpsy.org.au/modules/introducing-psychological-skills/': 'Elite sport performance/Introducing psychological skills/Introducing psychological skills',
      'http://www.elitesportpsy.org.au/modules/mental-training/#': 'Elite sport performance/Mental training/Mental training',
      'http://www.elitesportpsy.org.au/modules/mood-and-emotion-2/': 'Elite sport performance/Mood and emotion/Mood and emotion',
      'http://www.elitesportpsy.org.au/modules/motivation/': 'Elite sport performance/Motivation/Motivation',
      'http://www.elitesportpsy.org.au/modules/music/': 'Elite sport performance/Music/Music',
      'http://www.elitesportpsy.org.au/modules/self-confidence/': 'Elite sport performance/Self-confidence/Self-confidence',
      'http://www.elitesportpsy.org.au/topic/anxiety-summary/': 'Elite sport performance/Anxiety/Anxiety summary',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/': 'Elite sport performance/Self-confidence/Boosting self-confidence',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#a': 'Elite sport performance/Self-confidence/Boosting self-confidence#a',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#d': 'Elite sport performance/Self-confidence/Boosting self-confidence#d',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#p': 'Elite sport performance/Self-confidence/Boosting self-confidence#p',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#r': 'Elite sport performance/Self-confidence/Boosting self-confidence#r',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#u': 'Elite sport performance/Self-confidence/Boosting self-confidence#u',
      'http://www.elitesportpsy.org.au/topic/boosting-self-confidence/#w': 'Elite sport performance/Self-confidence/Boosting self-confidence#w',
      'http://www.elitesportpsy.org.au/topic/categories-of-psychological-influence/': 'Elite sport performance/Introduction/Categories of psychological influence',
      'http://www.elitesportpsy.org.au/topic/concentration-attention/': 'Elite sport performance/Concentration/Concentration attention',
      'http://www.elitesportpsy.org.au/topic/concentration-summary-and-go-further/': 'Elite sport performance/Concentration/Concentration summary and go further',
      'http://www.elitesportpsy.org.au/topic/concentration-training-techniques/': 'Elite sport performance/Concentration/Concentration training techniques',
      'http://www.elitesportpsy.org.au/topic/confidence-summary-and-go-further/': 'Elite sport performance/Self-confidence/Confidence summary and go further',
      'http://www.elitesportpsy.org.au/topic/controlling-anxiety/': 'Elite sport performance/Anxiety/Controlling anxiety',
      'http://www.elitesportpsy.org.au/topic/describing-motivation/': 'Elite sport performance/Motivation/Describing motivation',
      'http://www.elitesportpsy.org.au/topic/describing-motivation/#services1': 'Elite sport performance/Motivation/Describing motivation#services1',
      'http://www.elitesportpsy.org.au/topic/describing-motivation/#services2': 'Elite sport performance/Motivation/Describing motivation#services2',
      'http://www.elitesportpsy.org.au/topic/describing-motivation/#services3': 'Elite sport performance/Motivation/Describing motivation#services3',
      'http://www.elitesportpsy.org.au/topic/describing-motivation/#services4': 'Elite sport performance/Motivation/Describing motivation#services4',
      'http://www.elitesportpsy.org.au/topic/errors-and-distractions/': 'Elite sport performance/Concentration/Errors and distractions',
      'http://www.elitesportpsy.org.au/topic/group-dynamics/': 'Elite sport performance/Group dynamics/Group dynamics summary',
      'http://www.elitesportpsy.org.au/topic/imagery-for-skill-development-2/': 'Elite sport performance/Imagery/Imagery for skill development',
      'http://www.elitesportpsy.org.au/topic/imagery-in-competition/': 'Elite sport performance/Imagery/Imagery in competition',
      'http://www.elitesportpsy.org.au/topic/imagery-perspectives/': 'Elite sport performance/Imagery/Imagery perspectives',
      'http://www.elitesportpsy.org.au/topic/imagery-summary/': 'Elite sport performance/Imagery/Imagery summary',
      'http://www.elitesportpsy.org.au/topic/imagery-summary/golf-2/': 'Elite sport performance/Imagery/Imagery summary',
      'http://www.elitesportpsy.org.au/topic/improving-team-performance/': 'Elite sport performance/Group dynamics/Improving team performance',
      'http://www.elitesportpsy.org.au/topic/interventions-using-music/': 'Elite sport performance/Music/Interventions using music',
      'http://www.elitesportpsy.org.au/topic/introducing-group-dynamics/': 'Elite sport performance/Group dynamics/Introducing group dynamics',
      'http://www.elitesportpsy.org.au/topic/mood-emotion/': 'Elite sport performance/Mood and emotion/Mood and emotion',
      'http://www.elitesportpsy.org.au/topic/mood-regulation-strategies/': 'Elite sport performance/Mood and emotion/Mood regulation strategies',
      'http://www.elitesportpsy.org.au/topic/mood-summary-and-go-further/': 'Elite sport performance/Mood and emotion/Mood summary and go further',
      'http://www.elitesportpsy.org.au/topic/mood-summary-and-go-further/judo/': 'Elite sport performance/Mood and emotion/Mood summary and go further',
      'http://www.elitesportpsy.org.au/topic/motivation-summary/': 'Elite sport performance/Motivation/Motivation summary',
      'http://www.elitesportpsy.org.au/topic/motivational-strategies/': 'Elite sport performance/Motivation/Motivational strategies',
      'http://www.elitesportpsy.org.au/topic/music-performance/': 'Elite sport performance/Music/Music performance',
      'http://www.elitesportpsy.org.au/topic/music-summary-and-go-further/': 'Elite sport performance/Music/Music summary and go further',
      'http://www.elitesportpsy.org.au/topic/music-training/': 'Elite sport performance/Music/Music training',
      'http://www.elitesportpsy.org.au/topic/nature-of-self-confidence/': 'Elite sport performance/Self-confidence/Nature of self confidence',
      'http://www.elitesportpsy.org.au/topic/relaxation-techniques/': 'Elite sport performance/Anxiety/Relaxation techniques',
      'http://www.elitesportpsy.org.au/topic/responses-to-stress/': 'Elite sport performance/Anxiety/Responses to stress',
      'http://www.elitesportpsy.org.au/topic/sport-psychologist/': 'Elite sport performance/Sport psychologist',
      'http://www.elitesportpsy.org.au/topic/sport-psychologist/#': 'Elite sport performance/Introduction/Sport psychologist#',
      'http://www.elitesportpsy.org.au/topic/team-culture-and-distinctiveness/': 'Elite sport performance/Group dynamics/Team culture and distinctiveness',
      'http://www.elitesportpsy.org.au/topic/the-six-pillars-of-elite-sports-performance/': 'Elite sport performance/Introduction/The six pillars of elite sports performance',
      'http://www.elitesportpsy.org.au/topic/threats-to-self-confidence/': 'Elite sport performance/Self-confidence/Threats to self confidence',
      'http://www.elitesportpsy.org.au/topic/token-rewards/': 'Elite sport performance/Motivation/Token rewards',
      'http://www.elitesportpsy.org.au/topic/your-mood-profile/': 'Elite sport performance/Mood and emotion/Your mood profile',
      'http://www.elitesportpsy.org.au/topic/your-mood-profile/#': 'Elite sport performance/Mood and emotion/Your mood profile#'
    },
    links = {
    },
    token = '',
    WE;

function api(data, success, failure) {
  data.action = data.action || 'query';
  data.format = data.format || 'json';
  request.post({
    url: APIURL,
    jar: true,
    auth: {user: 'beta', pass: 'test'},
    form: data
  }, (err, httpResponse, body) => {
    if (err) {
      if (failure) {
        failure(err);
      } else {
        console.error('API error', err);
        process.exit(1);
      }
    } else {
      if (data.format === 'json') {
        body = JSON.parse(body);
      }
      success(body);
    }
  });
}

function processPages() {
  var page, p, file, basename, pagename;
  var $, $c, title, wikitext = '';

  if (pages.length === 0) {
    var exlinks = Object.keys(links);
    exlinks.sort();
    fs.writeFileSync('externallinks.txt', util.inspect(exlinks), 'utf-8');
    return;
  }

  page = pages.shift();
  console.log(pages.length, 'page', page);
  p = page.split('|');
  pagename = p[0];

  if (p.length == 1) {
    console.log(pagename);
    wikitext = 'Menu placeholder\n\n{{List subpages}}\n\n[[Category:CC-BY-4.0]]';
    api({action: 'edit', title: pagename, text: wikitext, summary: SUMMARY,
      token: token}, (d) => {
        console.log(' written', d);
        process.nextTick(processPages); // tail call
    });
  } else {
    basename = `${p[1]}`;
    file = fs.readFileSync(`html/${basename}.html`, 'utf-8');
    $ = cheerio.load(file);
    $c = $('article>.entry-content>.entry-text-wrap');
    $c.find('#learndash_next_prev_link, #learndash_uploaded_assignments, .learndash_topic_dots, .learndash_back_to_lesson, #learndash_back_to_lesson, .learndash_lesson_topics_list, .wp-caption, script').remove();

    // video
    $c.find('.videoplayer').each(function(ix, element) {
      var voptions = $(this).find('#options');
      var $creditp;
      if (voptions.length) {
        let video;
        voptions = JSON.parse(voptions.text());
        video = voptions.videos[0];
        console.log(video);

        if (video.youtubeID) {
          $(this).replaceWith(`{{YouTube|id=${video.youtubeID}|title=${video.title}}}`);
        } else if (video.vimeoID) {
          $(this).replaceWith(`{{Vimeo|ID=${video.vimeoID}}}{{Clear}}`);
        } else {
          $(this).replaceWith(`[[File:Video play icon.png|link=${video.mp4}]]`);
        }
        $('a').each(function() {
          if ($(this).text() === 'Video credit') {
            $creditp = $(this).closest('p');
          }
        });
        if ($creditp) {
          $creditp.remove();
        }
      } else {
        console.log('can not find video options');
      }
    });

    // images
    $c.find('img').each(function(ix, element) {
      var wikiimg = '',
          src = $(this).attr('src'),
          alt = $(this).attr('alt') || '',
          width = $(this).attr('width'),
          clear = '';
      wikiimg = `File:${path.basename(src)}`;
      if (alt) {
        wikiimg += `|${alt}`;
      }
      if (width) {
        wikiimg += `|${width}px`;
      }
      if ($(this).hasClass('alignright')) {
        wikiimg += `|right`;
      } else if ($(this).hasClass('alignone')) {
        clear = '{{Clear}}';
      }
      $(this).replaceWith(`[[${wikiimg}]]${clear}`);
    });

    // more
    $('.toggle').each(function() {
      var header = $(this).find('.toggle-label').text(),
          body = $(this).find('.toggle-content').html();
      $(this).replaceWith(`<div class="mw-collapsible mw-collapsed">${header}<div class="mw-collapsible-content">${body}</div></div>\n`);
    });

    // boxes
    $('.box-frame').each(function() {
      var header = $(this).find('.box-header').text(),
          body = $(this).find('.box-contents').html();
      $(this).replaceWith(`\n{{IDevice\n|theme=line\n|type=Activity\n|title=${header}\n|body=\n${body}\n}}\n`);
    });

    // triangle border
    $('.triangle-border').each(function() {
      var header = $(this).find('h3').text();
      $(this).find('h3').remove();
      $(this).replaceWith(`\n{{IDevice\n|theme=line\n|type=Reflection\n|title=${header}\n|body=\n${$(this).html()}\n}}\n`);
    });

    // slider
    $('.flexslider').append('{{#widget:Slider}}')
                    .find('.flex-control-nav,.flex-direction-nav').remove();
    $('.flexslider').find('.slides>li').each(function(){
      $(this).removeAttr('style')
             .removeClass('flex-active-slide');
    });

    // audio
    $c.find('audio').each(function() {
      $(this).replaceWith(`\n{{IDevice\n|theme=line\n|type=Media\n|body=\nAudio file goes here.}}\n`);
    });

    // links
    $c.find('a').each(function() {
      var href = $(this).attr('href'),
          anchor = $(this).html();
      if (targets.href) {
        $(this).replaceWith(`[[${targets.href}|${anchor}]]`);
      } else {
        if (href == undefined) {
          if ($(this).attr('name')) {
            console.log('name anchor');
          } else {
            console.log(`undefined href`);
          }
        } else {
          if (href.indexOf('elitesports') > -1) {
            console.log(` external link ${href}`);
          }
          links[href] = true;
          $(this).replaceWith(`[${href} ${anchor}]`);
        }
      }
    });

    // blockquotes
    $('blockquote').each(function() {
      var body = $(this).html().split('<br>');
      if (body.length === 2) {
        $(this).replaceWith(`\n{{Cquote|${body[0]}|author=${body[1]}}}\n`);
      }
    });

    // unwrap .learndash
    var $ld = $('div.learndash');
    $ld.replaceWith($ld.html());

    $('h2,h3').each(function() {
        var tag = $(this).tag,
            eq = (tag === 'h2') ? '==' : '===',
            text = $(this).text();
        $(this).replaceWith(`${eq} ${text} ${eq}\n`);
    });
    // get h1
    title = $('h1').text();
    wikitext = `= ${title} =\n__NOTOC__\n\n`;
    wikitext += $c.html();
    wikitext = wikitext.replace(/&nbsp;/g, ' ')
                       .replace(/&#xA0;/g, ' ')
                       .replace("’", "'")
                       .replace(/&#x2019;/g, "'")
                       .replace(/<p>/ig, "\n")
                       .replace(/<\/p>/ig, "");

    wikitext += '\n[[Category:CC-BY-4.0]]';

    fs.writeFileSync(`wikitext/${basename}.txt`, wikitext, 'utf-8');
    api({action: 'edit', title: pagename, text: wikitext, summary: SUMMARY,
      token: token}, (d) => { console.log(' written', d);});
    process.nextTick(processPages);
  }
}

try {
  WE = ini.parse(fs.readFileSync(untildify('~/.wikieducator.rc'), 'utf-8'));
} catch(e) {
  console.error('unable to load/parse ~/.wikieducator.rc login details', e);
  process.exit(1);
}
if (!WE.login || !WE.login.user || !WE.login.password) {
  console.error('~/.wikieducator.rc missing [login] section with user and password');
  process.exit(1);
}

api({action: 'login', lgname: WE.login.user, lgpassword: WE.login.password}, (d) => {
  console.log('login', d);
  console.log('d.login', d.login);
  api({action: 'login', lgname: WE.login.user, lgpassword: WE.login.password, lgtoken: d.login.token}, (d) => {
    console.log('login with token', d);
    api({prop: 'info', intoken: 'edit', titles: 'Elite sport performance'}, (d) => {
      for (let k in d.query.pages) {
        if (d.query.pages.hasOwnProperty(k)) {
          console.log('token:', d.query.pages[k]);
          token = d.query.pages[k].edittoken;
        }
      }
      console.log('token', token);
      processPages();
    });
  });
});


